from django.contrib import admin
from django.urls import path

from .views import FoodsAPIview, FoodCategoryAPIview, FilteredFoodAPIview

urlpatterns = [
    path('api/v1/Food', FoodsAPIview.as_view()),
    path('api/v1/FoodCategory', FoodCategoryAPIview.as_view()),
    path('api/v1/FoodFiltered', FilteredFoodAPIview.as_view()),
]