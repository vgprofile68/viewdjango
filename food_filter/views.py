from pickle import TRUE
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import generics
from .models import Food, FoodCategory
from .serializer import FoodSerializer, FoodListSerializer

# Create your views here.
class FoodsAPIview(generics.ListAPIView):
    queryset = Food.objects.all()
    serializer_class = FoodSerializer

class FoodCategoryAPIview(generics.ListAPIView):
    queryset = FoodCategory.objects.all()
    serializer_class = FoodListSerializer

class FilteredFoodAPIview(generics.ListAPIView):
    #Food_is_not_publish = Food.objects.filter(is_publish=False).all().select_related('category')
    #exlude_ids = [food.internal_code for food in Food_is_not_publish]
    #queryset = Food.objects.exclude(internal_code__in=exlude_ids).all()

    queryset = FoodCategory.objects.select_related(None).filter(food__is_publish=True).all()
    serializer_class = FoodListSerializer